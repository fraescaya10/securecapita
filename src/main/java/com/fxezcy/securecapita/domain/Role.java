package com.fxezcy.securecapita.domain;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class Role {
    private Long id;
    private String name;
    private String permissions;
}

package com.fxezcy.securecapita.repository.implementation;

import static com.fxezcy.securecapita.enumeration.RoleType.ROLE_USER;
import static com.fxezcy.securecapita.query.UserQuery.COUNT_USER_EMAIL_QUERY;
import static com.fxezcy.securecapita.query.UserQuery.INSERT_USER_QUERY;
import static java.util.Objects.requireNonNull;

import java.util.Collection;
import java.util.Map;

import javax.management.relation.Role;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.fxezcy.securecapita.domain.User;
import com.fxezcy.securecapita.exception.ApiException;
import com.fxezcy.securecapita.repository.RoleRepository;
import com.fxezcy.securecapita.repository.UserRepository;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Repository
@RequiredArgsConstructor
@Slf4j
public class UserRepositoryImpl implements UserRepository<User> {

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    private final RoleRepository<Role> roleRepository;

    @Override
    public User create(User user) {
        // Check the email is unique
        if (getEmailCount(user.getEmail().trim().toLowerCase()) > 0)
            throw new ApiException("Email already in use. Please use a different email and try again");
        // Save new user
        try {
            KeyHolder keyHolder = new GeneratedKeyHolder();
            SqlParameterSource sqlParameterSource = getSqlParameterSource(user);
            namedParameterJdbcTemplate.update(INSERT_USER_QUERY, sqlParameterSource, keyHolder);
            user.setId(requireNonNull(keyHolder.getKey()).longValue());
            roleRepository.addRoleToUser(user.getId(), ROLE_USER.name());
        } catch (EmptyResultDataAccessException ex) {
            // TODO: handle exception
        } catch (Exception ex) {
            // TODO: handle exception
        }
        // Add role to the user
        // Send verification url
        // Save url in verification table
        // Send email to user with verification url
        // Return the newley created user
        // If any errors, throw exception with proper message
        return null;
    }

    @Override
    public Collection list(int page, int pageSize) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'list'");
    }

    @Override
    public User get(Long id) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'get'");
    }

    @Override
    public User update(User data) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'update'");
    }

    @Override
    public Boolean delete(Long id) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'delete'");
    }

    private Integer getEmailCount(String email) {
        return namedParameterJdbcTemplate.queryForObject(COUNT_USER_EMAIL_QUERY, Map.of("email", email), Integer.class);
    }

    private SqlParameterSource getSqlParameterSource(User user) {
        return null;
    }

}

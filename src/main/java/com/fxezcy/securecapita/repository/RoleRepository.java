package com.fxezcy.securecapita.repository;

import java.util.Collection;

import javax.management.relation.Role;

public interface RoleRepository<T extends Role> {
        // Basic CRUD operations
    T create(T data);

    Collection<T> list(int page, int pageSize);

    T get(Long id);

    T update(T data);

    Boolean delete(Long id);

    // More complex operations
    void addRoleToUser(Long userId, String roleName);
}
